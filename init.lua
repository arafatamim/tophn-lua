--[[
-- Luvit v2.18
--]]

local thread = require("thread")

STORIES_LENGTH = 10

local function fetch_story(id)
	local https = require("https")

	local req = https.request({
		host = "hacker-news.firebaseio.com",
		port = 443,
		path = "/v0/item/" .. id .. ".json",
		method = "GET",
	}, function(res)
		local data = {}

		res:on("data", function(chunk)
			table.insert(data, chunk)
		end)

		res:on("end", function()
			local json = require("json")
			local story = json.decode(table.concat(data))
			print(
				story["title"]
					.. "\nScore: "
					.. story["score"]
					.. "\nBy: "
					.. story["by"]
					.. "\nURL: "
					.. story["url"]
					.. "\n"
			)
		end)
	end)

	req:done()
end

local req = require("https").request({
	host = "hacker-news.firebaseio.com",
	port = 443,
	path = "/v0/topstories.json",
	method = "GET",
}, function(res)
	local data = {}
	res:on("data", function(chunk)
		table.insert(data, chunk)
	end)

	res:on("end", function()
		local ids = require("json").decode(table.concat(data))
		local top_five = table.move(ids, 1, 10, 1, {})
		local threads = {}
		for _, value in ipairs(top_five) do
			local t = thread.start(fetch_story, value)
			table.insert(threads, t)
		end
		for _, t in ipairs(threads) do
			t:join()
		end
	end)
end)

req:done()
